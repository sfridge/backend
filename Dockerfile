# Base image 
FROM ruby:latest 

ENV HOME /home/rails/webapp 

# Install PGsql dependencies and js engine 
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs 

WORKDIR $HOME 
# Install gems 
ADD Gemfile* $HOME/ 
RUN bundle install --without development test

# Add the app code 
ADD . $HOME 

ENV RAILS_ENV=production
ENV APP_USER_POSTGRES_PASSWORD=ruby

# RUN rake db:create && rake db:migrate
# Default command 
CMD ["rails", "server", "--binding", "0.0.0.0", "-p", "3000"]