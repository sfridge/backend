class Api::V1::FridgeController < Api::V1::BaseController

	def index
		expose Fridge.all, each_serializer: FridgeSerializer
	end

	def show
		expose Fridge.find(params[:id]), serializer: FridgeSerializer
	end

	def create
		fridge = Fridge.create(fridge_params)
		params_category = params[:fridge][:category]
		if !params_category.nil?
			category = Category.find(params_category[:id])
			if !category.nil?
				fridge.category = category
			end
		end
		if fridge.save
			expose fridge, serializer: FridgeSerializer
		else
			head 500
			expose fridge.errors
		end
	end

	def update
		fridge = Fridge.find(params[:id])
		params_category = params[:fridge][:category]
		category = Category.find(params_category[:id])
		if !category.nil?
			fridge.category = category
		end
		if fridge.update(fridge_params)
			expose fridge, serializer: FridgeSerializer
		else
			head 500
			expose fridge.errors
		end
	end

	def destroy
		fridge = Fridge.find(params[:id])
		if !fridge.nil?
			fridge.delete
			head 204
		else
			error! :not_found
		end
	end

	def update_by_bar_code
		fridge = Fridge.find_by(bar_code: params[:search])

		if !fridge.nil?
			if fridge.update(fridge_params)
				expose fridge, serializer: FridgeSerializer
			else
				head 500
				expose fridge.errors
			end
		else
			error! :not_found
		end
	end

	def search_by_bar_code
		fridge = Fridge.find_by(bar_code: params[:search])

		if !fridge.nil?
			expose fridge, serializer: FridgeSerializer
		else
			error! :not_found
		end
	end

	def increase_quantity
		fridge = Fridge.find_by(bar_code: params[:bar_code])
		quantity = params[:quantity]
		if !fridge.nil?
			if !quantity.nil?
				fridge.quantity = quantity
			else
				fridge.quantity += 1
			end
			if fridge.save
				expose fridge
			else
				head 500
				expose fridge.errors
			end
		else
			error! :not_found
		end
	end

	def decrease_quantity
		fridge = Fridge.find_by(bar_code: params[:bar_code])
		quantity = params[:quantity]
		if !fridge.nil?
			if fridge.quantity <= 0
				head 500
				expose "no quantity"
			else
				if !quantity.nil?
					fridge.quantity = quantity
				else
					fridge.quantity -= 1
				end
				if fridge.save
					expose fridge
				else
					head 500
					expose fridge.errors
				end
			end
		else
			error! :not_found
		end
	end

	def product_exist_by_bar_code
		fridge = Fridge.find_by(bar_code: params[:bar_code])

		if !fridge.nil?
			expose true
		else
			expose false
		end
	end


	private

	def fridge_params
		params.require(:fridge).permit(:name, :bar_code, :quantity, :must_have, :created_at, :expiration_date, :category)
	end

end
