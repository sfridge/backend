class Api::V1::CategoryController < Api::V1::BaseController

	def index
		expose Category.all, each_serializer: CategorySerializer
	end

	def show
		expose Category.find(params[:id]), serializer: CategorySerializer
	end

	def create
		category = Category.create(category_params)
		if category.save
			expose category
		else
			expose category.errors
		end
	end

	def update
		category = Category.find(params[:id])

		if category.update(category_params)
			expose category
		else
			expose category.errors
		end
	end

	def destroy
		category = Category.find(params[:id])
		if !category.nil?
			category.delete
			head 204
		else
			expose "Category doesn't exist"
		end
	end

	private

	def category_params
		params.require(:category).permit(:name, :expiration_delay)
	end

end
