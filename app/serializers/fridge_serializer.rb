class FridgeSerializer < ActiveModel::Serializer
  attributes :id, :name, :bar_code, :quantity, :must_have, :created_at, :expiration_date, :category
end
