class CategorySerializer < ActiveModel::Serializer
  attributes :id, :expiration_delay, :name, :fridges
end
