class Fridge < ActiveRecord::Base
	belongs_to :category

	validates :bar_code, uniqueness: true
end
