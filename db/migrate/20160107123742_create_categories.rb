class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :expiration_delay

      t.timestamps null: false
    end
  end
end
