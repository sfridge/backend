class CreateFridges < ActiveRecord::Migration
  def change
    create_table :fridges do |t|
      t.string :name, index: true,  null: false
      t.string :bar_code, unique: true, index: true, null: false
      t.integer :quantity, null: false
      t.boolean :must_have, default: false, null: false
      t.datetime :expiration_date
      t.references :category

      t.timestamps null: false
    end
  end
end
