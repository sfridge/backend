class CreateBuyingLists < ActiveRecord::Migration
  def change
    create_table :buying_lists do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
