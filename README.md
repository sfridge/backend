# Server url : backend.sfridge.de-coster.fr
|Name in ruby      			|Prefix Verb   		|URI Pattern                                |Controller#Action 											|Comment
|---------------------------|-------------------|-------------------------------------------|-----------------------------------------------------------|-----------------------------------|
|api_category_index 		|GET    			|/api/category(.:format)                    |api/v1/category#index {:format=>"json"}					|									|
|                      		|POST   			|/api/category(.:format)                    |api/v1/category#create {:format=>"json"}					|									|
|api_category 				|GET    			|/api/category/:id(.:format)                |api/v1/category#show {:format=>"json"}						|									|
|                      		|PATCH  			|/api/category/:id(.:format)                |api/v1/category#update {:format=>"json"}					|									|
|                      		|PUT    			|/api/category/:id(.:format)                |api/v1/category#update {:format=>"json"}					|									|
|                      		|DELETE 			|/api/category/:id(.:format)                |api/v1/category#destroy {:format=>"json"}					|									|
|api_fridge_index 			|GET    			|/api/fridge(.:format)                      |api/v1/fridge#index {:format=>"json"}						|									|
|                      		|POST   			|/api/fridge(.:format)                      |api/v1/fridge#create {:format=>"json"}						|									|
|api_fridge 				|GET    			|/api/fridge/:id(.:format)                  |api/v1/fridge#show {:format=>"json"}						|									|
|                      		|PATCH  			|/api/fridge/:id(.:format)                  |api/v1/fridge#update {:format=>"json"}						|									|
|                      		|PUT    			|/api/fridge/:id(.:format)                  |api/v1/fridge#update {:format=>"json"}						|									|
|	                    	|DELETE 			|/api/fridge/:id(.:format)                  |api/v1/fridge#destroy {:format=>"json"}					|									|
|api_home_index 			|GET    			|/api/home(.:format)                        |api/v1/home#index {:format=>"json"}						|									|
|api_search_bar_code 		|GET    			|/api/bar_code(.:format)                    |api/v1/fridge#search_by_bar_code {:format=>"json"}			|									|
|api_update_bar_code 		|PUT    			|/api/bar_code(.:format)                    |api/v1/fridge#update_by_bar_code {:format=>"json"}			|									|
|api_increase_quantity 		|PUT    			|/api/bar_code/:bar_code/increase(.:format) |api/v1/fridge#increase_quantity {:format=>"json"}			|params optional quantity=number	|
|api_decrease_quantity 		|PUT    			|/api/bar_code/:bar_code/decrease(.:format) |api/v1/fridge#decrease_quantity {:format=>"json"}			|params optional quantity=number	|
|api_product_exist 			|GET    			|/api/bar_code/exist/:bar_code(.:format)    |api/v1/fridge#product_exist_by_bar_code {:format=>"json"}	|									|
|api_shopping_list |GET    |/api/shopping_list(.:format)               |api/v1/fridge#get_to_buy {:format=>"json"}|
    |api_shopping_list_empty |GET    |/api/shopping_list/empty(.:format)         |api/v1/fridge#get_item_quantity_zero {:format=>"json"}|
|api_shopping_list_must_have |GET    |/api/shopping_list/must_have(.:format)     |api/v1/fridge#get_item_must_have {:format=>"json"}|
    |api_shopping_list_debug GET    |/api/shopping_list/debug(.:format)         |api/v1/fridge#get_empty_or_must_have {:format=>"json"}|