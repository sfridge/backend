Rails.application.routes.draw do
  api_version(:module => "Api::V1", :path => {:value => "/api"}, :defaults => {:format => "json"}) do
    resources :category, only: [:index, :show, :create, :update, :destroy]
    resources :fridge, only: [:index, :show, :create, :update, :destroy] 
    resources :home, only: [:index]
    get '/bar_code', to: 'fridge#search_by_bar_code', as: "search_bar_code"
    put '/bar_code', to: 'fridge#update_by_bar_code', as: "update_bar_code"
    put '/bar_code/:bar_code/increase', to: 'fridge#increase_quantity', as: "increase_quantity"
    put '/bar_code/:bar_code/decrease', to: 'fridge#decrease_quantity', as: "decrease_quantity"
    get '/bar_code/exist/:bar_code', to: 'fridge#product_exist_by_bar_code', as: "product_exist"
  end
end
